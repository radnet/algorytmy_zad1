﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad1
{
    class Program
    {

        static void Main(string[] args)
        {

            Program pro = new Program();
            pro.readData();

        }

        private void readData()
        {
            int testCount = Convert.ToInt32(Console.ReadLine());

            for (int t = 0; t < testCount; t++)
            {
                int nodeCount = Convert.ToInt32(Console.ReadLine());

                int[,] matrix = new int[nodeCount, nodeCount];

                for (int i = 0; i < nodeCount; i++)
                {
                    string line = Console.ReadLine();
                    string[] cols = line.Split(' ');
                    for (int j = 0; j < nodeCount; j++)
                    {
                        matrix[i, j] = Convert.ToInt32(cols[j]);
                    }
                }

                new GraphInfo(t, matrix, nodeCount);
            }

        }

        class GraphInfo
        {
            int ID; 
            int N; 
            int[,] matrix;
            bool[] visited;
            int[] COLOR_1; // dwudzielny
            int[] COLOR_2; // cykliczny
            int componentCount;
            bool isCyclic = false;
            bool isBipartite = false;


            public GraphInfo(int ID,  int[,] matrix, int N)
            {
                this.ID = ID + 1;
                this.matrix = matrix;
                this.N = N;
                init();
                start();
                output();
            }

            private void init()
            {
                visited = new bool[N];
                COLOR_1 = new int[N];
                COLOR_2 = new int[N];

                for (int i = 0; i < N; i++)
                {
                    visited[i] = false;
                    COLOR_1[i] = 0;
                    COLOR_2[i] = 0;
                }
            }

            private void start()
            {
                int[,] w = matrix;
                bool dir = checkIfDirected();
                if (dir)
                    w = getSymmetricMatrix();

                for (int i = 0; i < N; i++)
                {
                    if (visited[i] == false)
                    {
                        componentCount++;
                        COLOR_1[i] = 1;
                        DFS(w, i, i);
                    }
                }

                bool isEdge = false;
                for (int i = 0; i < N - 1; i++)
                {
                    for (int j = i + 1; j < N; j++)
                    {
                        if (w[i, j] == 1)
                        {
                            if(!isEdge)
                            {
                                isBipartite = true;
                                isEdge = true;
                            }
                            
                            if (COLOR_1[i] == COLOR_1[j])
                            {
                                isBipartite = false;
                                break;
                            }
                        }
                    }
                }

                if (dir)
                {
                    w = matrix;
                    init();
                    isCyclic = false;
                    for (int i = 0; i < N; i++)
                    {
                        if (visited[i] == false)
                        {
                            DFS_Dir(w, i);
                        }
                    }
                }    

            }

            private bool checkIfDirected()
            {
                for (int i = 0; i < N; i++)
                {
                    for (int j = 0; j < N; j++)
                    {
                        if (matrix[i, j] != matrix[j, i])
                        {
                            return true;
                        }
                    }
                }
                return false;
            }

            private int[,] getSymmetricMatrix()
            {
                int [,] newMatrix = new int[N, N];
                for (int i = 0; i < N; i++)
                {
                    for (int j = 0; j < N; j++)
                    {
                        if (matrix[i, j] == 1 || matrix[j, i] == 1)
                        {
                            newMatrix[i, j] = 1;
                            newMatrix[j, i] = 1;
                        }
                    }
                }
                return newMatrix;
            }

            private void DFS(int[,] w, int v, int p)
            {
                visited[v] = true;
                for (int i = 0; i < N; i++)
                {
                    if (w[v, i] == 1)
                    {
                        if (visited[i] == false)
                        {
                            if (COLOR_1[v] == 1)
                            {
                                COLOR_1[i] = 2;
                            }
                            else
                            {
                                COLOR_1[i] = 1;
                            }
                        }
                    }
                }
                for (int i = 0; i < N; i++)
                {
                    if (w[v, i] == 1)
                    {
                        if (visited[i] == false)
                        {
                            DFS(w, i, v);
                        }
                        else
                        {
                            if (p != i)
                            {
                                isCyclic = true;
                            }

                        }
                    }
                }
            }

            void DFS_Dir(int[,] w, int v)
            {
                visited[v] = true;
                COLOR_2[v] = 1;

                for (int i = 0; i < N; i++)
                {
                    if (w[v, i] == 1)
                    {
                        if (visited[i] == false)
                        {
                            DFS_Dir(w, i);
                        }
                        else
                        {
                            if (COLOR_2[i] == 1)
                            {
                                isCyclic = true;
                            }
                        }
                    }
                }
                COLOR_2[v] = 2;
            }


            private void output()
            {

                Console.WriteLine("Graf " + ID);

                if (isBipartite == true)
                    Console.WriteLine("Dwudzielny TAK");
                else
                    Console.WriteLine("Dwudzielny NIE");

                if (componentCount == 1)
                    Console.WriteLine("Spojny TAK (1)");
                else
                    Console.WriteLine("Spojny NIE (" + componentCount + ")");

                if (isCyclic)
                    Console.WriteLine("Cykle TAK");
                else
                    Console.WriteLine("Cykle NIE");

                if (componentCount == 1 && isCyclic == false)
                    Console.WriteLine("Drzewo TAK");
                else
                    Console.WriteLine("Drzewo NIE");
            }
        }


    }
}
